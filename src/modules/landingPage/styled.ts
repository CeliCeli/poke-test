import styled, { css } from "styled-components/macro";
import bg from "assets/images/bg.jpg";
import { ScInputGroup } from "components/Input/styled";
import { ScSelect } from "components/Select/styled";

export const Container = styled.div<{ isOpen?: boolean }>`
  background: url(${bg}) center bottom no-repeat;
  background-attachment: fixed;
  background-size: cover;
  min-height: 100vh;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  z-index: 1;
  &:before {
    position: absolute;
    /* content: ""; */
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: rgba(0, 0, 0, 0.7);
    z-index: -1;
  }

  ${({ isOpen }) =>
    isOpen &&
    css`
      overflow: hidden;
      max-height: calc(100vh - 20px);
    `}
`;

export const ScPagination = styled.div`
  display: flex;
  justify-content: center;
  padding: 24px 0;
  flex-wrap: wrap;
  max-width: 767px;
  margin: 0 auto;
  .inputWrap,
  .buttonWrap {
    flex: 0 0 100%;
    max-width: 100%;
    display: flex;
    justify-content: center;
    margin: 0 0 32px 0;
  }

  ${ScInputGroup} {
    margin: 0;
    width: auto;
    input[type="text"] {
      height: 46px;
      margin: 0 10px;
      font-size: 25px;
      max-width: 180px;
      font-family: "VT323", monospace;
      box-shadow: 4px 0 0 0 black, -4px 0 0 0 black, 0 4px 0 0 black,
        0 -4px 0 0 black;
    }
  }

  @media (max-width: 577px){
    .inputWrap{
      flex-direction: column;
      align-items: center;
    }
    ${ScSelect}{
      margin-bottom: 32px;
    }
  }
`;
