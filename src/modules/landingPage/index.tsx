import { Dispatch, useEffect, useState, useMemo } from "react";
import { useDispatch } from "react-redux";
import { PokemonType } from "types/items";
import { fetchCurrentItem, fetchItems } from "store/actions/items";
import { ItemList as Lobbby } from "modules/ItemList/index";

import { Container, ScPagination } from "./styled";
import { Button } from "components/Button";
import { theme } from "../../theme/index";
import { fetchTypes } from "store/actions/types";
import { useTypedSelector } from "hooks/useTypedSelector";
import { Select } from "components/Select";
import { ScInputGroup } from "components/Input/styled";

export const Landing = () => {
  const dispatch: Dispatch<any> = useDispatch();
  const types = useTypedSelector((state) => state.types.data);
  const {
    data: items,
    error,
    loading,
  } = useTypedSelector((state) => state.items);
  const [sortParam, setSortParam] = useState("");
  const [searchParam, setSearchParam] = useState("");
  const [offset, setOffset] = useState<number>(0);

  const data = useMemo((): PokemonType[] => {
    if (sortParam.length === 0) return items;
    return items.filter((el) => el.type?.includes(sortParam));
  }, [items, sortParam]);

  useEffect(() => {
    dispatch(fetchTypes());
    dispatch(
      fetchItems(
        sortParam.length === 0 ? offset : 100,
        sortParam.length === 0 ? 9 : 100
      )
    );
  }, [offset, sortParam]);

  const cleanFunc = () => {
    setSortParam("");
    setOffset(0);
  };

  const search = () => {
    if (searchParam.length === 0) {
      setSortParam("");
      dispatch(fetchItems(offset, 9));
      return;
    }
    dispatch(fetchCurrentItem(searchParam));
  };

  const isBlockedPagination = useMemo(() => {
    return data.length !== 9;
  }, [data, sortParam]);

  return (
    <Container>
      <ScPagination>
        <div className="inputWrap">
          {types.length !== 0 && (
            <Select
              options={["all", ...types]}
              value={sortParam.length !== 0 ? sortParam : "all"}
              onSet={(value) => setSortParam(value === "all" ? "" : value)}
            />
          )}
          <ScInputGroup>
            <input
              type="text"
              onChange={(e) => {
                cleanFunc();
                setSearchParam(e.currentTarget.value);
              }}
            />
            <Button color={theme.colors.orange} onClick={search}>
              search
            </Button>
          </ScInputGroup>
        </div>
        <div className="buttonWrap">
          <Button
            color={theme.colors.orange}
            onClick={() => setOffset((s) => (s !== 0 ? s - 1 : s))}
            disabled={isBlockedPagination || offset === 0}
          >
            back
          </Button>
          <Button
            color={theme.colors.primaryColor}
            onClick={() => setOffset((s) => s + 1)}
            disabled={isBlockedPagination}
          >
            next
          </Button>
        </div>
      </ScPagination>
      {data.length !== 0 ? (
        <Lobbby data={data} error={error} loading={loading} />
      ) : (
        <>emptyComponent</>
      )}
    </Container>
  );
};
