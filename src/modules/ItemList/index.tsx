import { FC } from "react";
import { PokemonType } from "types/items";
import { Pokemon } from "components/Pokemon";
import { ScItemList, ScItemWrap } from "./styled";
import { ScGridContainer, ScGridElement } from "styles/general";

interface ItemListProps {
  data: PokemonType[];
  error: string | null;
  loading: boolean;
}

export const ItemList: FC<ItemListProps> = ({ data, error, loading }) => {
  return (
    <ScItemList>
      {((error || loading) && <>LoaderComponent</>) || (
        <>
          <ScItemWrap>
            <ScGridContainer gap={16} grid={3}>
              {data.map((el, index) => {
                return (
                  <ScGridElement key={`${el.name}.${index}`}>
                    <Pokemon
                      info={el.info}
                      name={el.name}
                      type={el.type}
                      img={el.img}
                    />
                  </ScGridElement>
                );
              })}
            </ScGridContainer>
          </ScItemWrap>
        </>
      )}
    </ScItemList>
  );
};
