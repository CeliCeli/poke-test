import styled from "styled-components/macro";

import { hexToRGB } from "helpers/hexToRgb";
import { theme } from "theme";

export const ScItemList = styled.div`
  box-sizing: border-box;
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 798px;
  font-family: 'VT323', monospace;
  padding: 0;

  .backdrop {
    position: absolute;
    z-index: 9;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    background: ${(hexToRGB(theme.colors.secondaryColor1), 5)};
    backdrop-filter: blur(10px);
  }

  @media (max-width: 991px){
    min-height: 0;
  }
`;

export const ScItemWrap = styled.div`
  flex: 0 0 767px;
  max-width: 767px;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  align-items: center;

  @media (max-width: 991px){
    flex: 0 0 100%;
    max-width: 577px;
  }
`;
