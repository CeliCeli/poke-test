import { Provider } from "react-redux";
import store from "store";
import { Landing } from "./modules/landingPage";
import GlobalStyle from "./styles/global";

const App = () => {
  return (
    <>
      <GlobalStyle />
      <Landing />
    </>
  );
};

export default App;
