import { composeWithDevTools } from "redux-devtools-extension";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import rootReducer from "./redusers";
import { configureStore } from "@reduxjs/toolkit";

const middleware = [thunk];

const store = configureStore({
  reducer: rootReducer,
  middleware,
});

export default store;

export type RootState = ReturnType<typeof rootReducer>;
