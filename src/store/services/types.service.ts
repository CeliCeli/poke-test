import axios from "axios";
import { DefaultType } from "types";

export const getItemTypes = async () => {
  const data = await axios
    .get("https://pokeapi.co/api/v2/type")
    .then((r) => r.data.results);
  return data;
};
