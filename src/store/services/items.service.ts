import axios from "axios";
import { PokemonType } from "types/items";

export const getItemList = async (offset: number = 0, limit: number) => {
  return await axios
    .get(`https://pokeapi.co/api/v2/pokemon`, {
      params: { offset: offset >= limit ? 0 : offset * limit, limit: limit },
    })
    .then((r) => r.data.results);
};

export const getCurrentPokemon = async (name: string) => {
  return await axios
    .get(`https://pokeapi.co/api/v2/pokemon/${name.toLowerCase()}`, {})
    .then((r) => r.data);
};

type TSearchParam = {
  url: string;
};

export const getCurrentItem = async <
  T extends TSearchParam,
  U extends PokemonType
>(
  data: T[]
) => {
  let arr: U[] = [];
  for (let i = 0; i < data.length; i++) {
    try {
      await axios.get(data[i].url).then((r) => arr.push(r.data));
    } catch (e) {
      console.log(e);
    }
  }

  return arr;
};

export const createObject = (data: any[]): PokemonType[] => {
  let arr: PokemonType[] = [];
  data.map((el) =>
    arr.push({
      name: el.name,
      img: el.sprites.front_default,
      type: el.types.map((el: { type: { name: any } }) => el.type.name),
      info: {
        moves: [el.moves[0].move, el.moves[1].move, el.moves[2].move],
        stats: el.stats,
      },
    })
  );
  return arr;
};

export const getData = async (names: any[], type?: string) => {
  const fullObj = await getCurrentItem(names).then((res) => res);
  return createObject(fullObj);
};
