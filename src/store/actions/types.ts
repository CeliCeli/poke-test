import { Dispatch } from "react";
import { getItemTypes } from "store/services/types.service";
import { DefaultType } from "types";
import { TTypesAction, TypesActions } from "types/types";

export const fetchTypes = () => {
  return async (dispatch: Dispatch<TTypesAction>) => {
    try {
      dispatch({ type: TypesActions.FETCH_TYPES });
      const arr: DefaultType[] = await getItemTypes().then((res) => res);
      const data = arr.map((el) => el.name);
      
      dispatch({
        type: TypesActions.FETCH_TYPES_SUCCESS,
        payload: data,
      });
    } catch (e) {
      dispatch({
        type: TypesActions.FETCH_TYPES_ERROR,
        payload: `Ошибка при загрузке данных`,
      });
    }
  };
};
