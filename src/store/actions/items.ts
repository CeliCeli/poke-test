import { Dispatch } from "react";
import { ItemsActions, ItemsActionType } from "types/items";
import {
  createObject,
  getData,
  getItemList,
} from "store/services/items.service";
import { getCurrentPokemon } from "../services/items.service";

export const fetchItems = (limit = 3, offset = 0) => {
  return async (dispatch: Dispatch<ItemsActionType>) => {
    try {
      dispatch({ type: ItemsActions.FETCH_ITEMS });
      const names = await getItemList(limit, offset).then((res) => res);
      const data = await getData(names).then((res) => res);

      dispatch({
        type: ItemsActions.FETCH_ITEMS_SUCCESS,
        payload: data,
      });
    } catch (e) {
      dispatch({
        type: ItemsActions.FETCH_ITEMS_ERROR,
        payload: `Ошибка при загрузке данных`,
      });
    }
  };
};

export const fetchCurrentItem = (url: string) => {
  return async (dispatch: Dispatch<ItemsActionType>) => {
    try {
      const item = await getCurrentPokemon(url).then((res) => res);

      const data = createObject([item]);

      dispatch({
        type: ItemsActions.FETCH_CURRENT_ITEM,
        payload: data,
      });
    } catch (e) {
      dispatch({
        type: ItemsActions.FETCH_ITEMS_ERROR,
        payload: `Не найдено`,
      });
    }
  };
};
