import { combineReducers } from "redux";
import { itemReducer as items } from "./items";
import { typesReducer as types } from "./types";

export default combineReducers({
  types,
  items,
});
