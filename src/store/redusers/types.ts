import { TTypesAction, TypesActions, TypesState } from "types/types";

const initialState: TypesState = {
  data: [],
  loading: false,
  error: null,
};

export const typesReducer = (
  state: TypesState = initialState,
  action: TTypesAction
): TypesState => {
  switch (action.type) {
    case TypesActions.FETCH_TYPES:
      return { ...state, loading: true };

    case TypesActions.FETCH_TYPES_SUCCESS:
      return { ...state, loading: false, data: action.payload };

    case TypesActions.FETCH_TYPES_ERROR:
      return { ...state, loading: false, error: action.payload };

    default:
      return state;
  }
};
