import { ItemsActionType, Itemstate, ItemsActions } from "types/items";

const initialState: Itemstate = {
  data: [],
  limit: 10,
  loading: false,
  error: null,
};

export const itemReducer = (
  state: Itemstate = initialState,
  action: ItemsActionType
): Itemstate => {
  switch (action.type) {
    case ItemsActions.FETCH_ITEMS:
      return { ...state, loading: true };
    case ItemsActions.FETCH_ITEMS_SUCCESS:
      return { ...state, loading: false, data: action.payload };
    case ItemsActions.FETCH_ITEMS_ERROR:
      return { ...state, loading: false, error: action.payload };
    case ItemsActions.FETCH_CURRENT_ITEM:
      return { ...state, loading: false, data: action.payload };
    default:
      return state;
  }
};
