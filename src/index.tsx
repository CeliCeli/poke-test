import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import DocumentMeta from "react-document-meta";
import { Provider } from "react-redux";
import store from "store";

const meta = {
  title: "title",
  description: "description",
  meta: {
    charset: "utf-8",
    name: {
      keywords: "keywords",
    },
  },
};

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);

root.render(
  <DocumentMeta {...meta}>
    <Provider store={store}>
      <React.StrictMode>
        <App />
      </React.StrictMode>
    </Provider>
  </DocumentMeta>
);

reportWebVitals();
