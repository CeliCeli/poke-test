import { DefaultTheme } from "@react-navigation/native";

export const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    witeColor: "#ffffff",
    primaryColor: "#04D0CA",
    secondaryColor1: "#1B1B1B",
    secondaryColor2: "#F4F4F4",
    orange: "#F45E01",
    pink: "#C60394",
    yellow: "#FFE725",
  },
};
