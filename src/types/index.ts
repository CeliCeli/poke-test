import rootReducer from "../store/redusers/index";

export type TReducerState = ReturnType<typeof rootReducer>;

export type DefaultType = {
  name: string;
  url: string;
};
