import { DefaultType } from "./index";
export interface TypesState {
  data: string[];
  loading: boolean;
  error: string | null;
}

export enum TypesActions {
  FETCH_TYPES = 'FETCH_TYPES',
  FETCH_TYPES_SUCCESS = 'FETCH_TYPES_SUCCESS',
  FETCH_TYPES_ERROR = 'FETCH_TYPES_ERROR',
}

interface FetchTypesAction {
  type: TypesActions.FETCH_TYPES;
}
interface FetchTypesActionSuccess {
  type: TypesActions.FETCH_TYPES_SUCCESS;
  payload: string[];
}
interface FetchTypesActionError {
  type: TypesActions.FETCH_TYPES_ERROR;
  payload: string;
}

export type TTypesAction =
  | FetchTypesAction
  | FetchTypesActionSuccess
  | FetchTypesActionError;
