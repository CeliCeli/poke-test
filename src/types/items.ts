import { DefaultType } from "types";

export const enum ItemsActions {
  FETCH_ITEMS = 'FETCH_ITEMS',
  FETCH_ITEMS_SUCCESS= 'FETCH_ITEMS_SUCCESS',
  FETCH_ITEMS_ERROR= 'FETCH_ITEMS_ERROR',
  FETCH_CURRENT_ITEM= 'FETCH_CURRENT_ITEM',
}

export type PokemonType = {
  type: string[];
  name: string;
  img: string;
  info: PokemonInfoType;
};

interface FetchItemsAction {
  type: ItemsActions.FETCH_ITEMS;
}
interface FetchItemsActionSuccess {
  type: ItemsActions.FETCH_ITEMS_SUCCESS;
  payload: PokemonType[];
}
interface FetchItemsActionError {
  type: ItemsActions.FETCH_ITEMS_ERROR;
  payload: string;
}

interface FetchCurrentItem {
  type: ItemsActions.FETCH_CURRENT_ITEM;
  payload: PokemonType[];
}

export interface Itemstate {
  data: any[];
  limit: number;
  loading: boolean;
  error: null | string;
}

type TSpecs = {
  stat: { name: string };
  base_stat: number;
};

type TMoves = DefaultType;

type PokemonInfoType = {
  moves: TMoves[];
  stats: TSpecs[];
};

export type ItemsActionType =
  | FetchItemsAction
  | FetchItemsActionSuccess
  | FetchItemsActionError
  | FetchCurrentItem;
