import { FC, ReactNode } from "react";
import { ScButton, ScButtonProps } from "./styled";

interface ButtonProps extends ScButtonProps {
  children: ReactNode;
  onClick?: () => void;
}

export const Button: FC<ButtonProps> = ({
  children,
  onClick,
  color,
  fontColor,
  disabled,
}) => {
  return (
    <ScButton
      disabled={disabled}
      onClick={onClick}
      color={color}
      fontColor={fontColor}
    >
      {children}
    </ScButton>
  );
};
