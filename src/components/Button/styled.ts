import styled, { css } from "styled-components/macro";
import { theme } from "theme";

export interface ScButtonProps {
  color?: string;
  fontColor?: string;
  disabled?: boolean;
}
export const ScButton = styled.button<ScButtonProps>`
  position: relative;
  display: block;
  margin: 0 16px;
  text-transform: uppercase;
  font-size: 25px;
  font-family: "VT323", monospace;
  color: white;
  padding: 4px 12px;
  background: ${theme.colors.secondaryColor1};
  width: auto;
  z-index: 2;
  border: none;
  cursor: pointer;
  &:active {
    top: 2px;
  }
  &:before,
  &:after {
    position: absolute;
    content: "";
    display: block;
    z-index: -1;
  }
  &:before {
    top: 10px;
    bottom: 10px;
    left: -10px;
    right: -10px;
    background: ${theme.colors.secondaryColor1};
  }
  &:after {
    top: 4px;
    bottom: 4px;
    left: -6px;
    right: -6px;
    background: ${theme.colors.secondaryColor1};
  }

  ${({ color }) =>
    color &&
    css`
      background: ${color};
      color: ${theme.colors.secondaryColor1};
      &:after,
      &:before {
        background: ${color};
      }
    `}

  ${({ fontColor }) =>
    fontColor &&
    css`
      color: ${fontColor};
    `}

  ${({ disabled }) =>
    disabled &&
    css`
      filter: grayscale(1);
      pointer-events: none;
    `}
`;
