import { FC, useState, useRef, useEffect } from "react";
import { ScBall, ScCard, ScPokemon } from "./styled";
import { PokemonType } from "types/items";

interface PokemonProps extends PokemonType {
  onClick?: () => void;
}

export const Pokemon: FC<PokemonProps> = ({
  name,
  info,
  type,
  img,
  onClick,
}) => {
  const ref = useRef<HTMLDivElement>(null);
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    const handleClickOutside = (event: any) => {
      let target = event.target as HTMLDivElement;

      if (ref.current && !ref.current.contains(target) && isOpen) {
        setIsOpen(false);
      }
    };
    document.addEventListener("mousedown", (e) => handleClickOutside(e));
    return () => {
      document.removeEventListener("mousedown", (e) => handleClickOutside(e));
    };
  }, [ref, isOpen]);

  return (
    <ScPokemon isOpen={isOpen} ref={ref}>
      {isOpen && (
        <ScCard
          isAnimate={false}
          onClick={() => {
            onClick && onClick();
            setIsOpen((s) => !s);
          }}
        >
          <span className="name">{`${name}`}</span>
          <div className="imgWrp">
            <img src={img} alt={name} />
          </div>
          {type && (
            <>
              <div className="headline">Type: </div>
              <ul>
                {type.map((el, index) => (
                  <li key={index}>{el}</li>
                ))}
              </ul>
            </>
          )}
          {info.moves && (
            <>
              <div className="headline">Moves: </div>
              <ul>
                {info.moves.map((el, index) => (
                  <li key={index}>{el.name}</li>
                ))}
              </ul>
            </>
          )}
          {info.stats && (
            <div className="stats">
              <div className="headline">Stats: </div>
              <ul>
                {info.stats
                  .filter(
                    (el) =>
                      el.stat.name !== "special-attack" &&
                      el.stat.name !== "special-defense"
                  )
                  .map((el, index) => (
                    <li className="statItem" key={index}>
                      <span className="title">{el.stat.name}</span>
                      <span
                        className="bar"
                        style={{ width: `${el.base_stat / 2}%` }}
                      >
                        <span>{el.base_stat}/200</span>
                      </span>
                    </li>
                  ))}
              </ul>
            </div>
          )}
        </ScCard>
      )}
      <ScBall
        onClick={() => {
          onClick && onClick();
          setIsOpen((s) => !s);
        }}
      >
        <div className="shape" />
      </ScBall>
      <span className="name">{name}</span>
    </ScPokemon>
  );
};
