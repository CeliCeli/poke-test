import styled, { css, keyframes } from "styled-components/macro";
import { theme } from "theme";
import { hexToRGB } from "helpers/hexToRgb";

import cardBg from "assets/images/card.png";

const show = keyframes`
  to{
    opacity: 1;
  }
`;

export const ScBall = styled.div`
  position: relative;
  width: 200px;
  height: 200px;
  background: #fff;
  border: 10px solid #000;
  border-radius: 50%;
  overflow: hidden;
  box-shadow: inset -10px 10px 0 10px #ccc,
    0px 0px 64px ${hexToRGB("#FFD12E", 0.0)};

  opacity: 0;
  animation: ${show} 1s 0.1s ease forwards;
  &::before,
  &::after {
    content: "";
    position: absolute;
  }
  &::before {
    background: red;
    width: 100%;
    height: 50%;
  }
  &::after {
    top: calc(50% - 10px);
    width: 100%;
    height: 24px;
    background: #000;
  }
  .shape {
    position: absolute;
    top: calc(50% - 30px);
    left: calc(50% - 30px);
    width: 60px;
    height: 60px;
    background: #7f8c8d;
    border: 10px solid #fff;
    box-sizing: border-box;
    border-radius: 50%;
    z-index: 10;
    box-shadow: 0 0 0 12px black;
  }

  @media (max-width: 577px) {
    width: calc(100% - 20px);
    height: auto;
    min-width: 200px;
    aspect-ratio: 1/1;
  }
`;

export const ScPokemon = styled.div<{ isOpen?: boolean }>`
  cursor: pointer;
  padding: 0 0 46px 0;
  position: relative;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  .name {
    /* background: ${theme.colors.witeColor}; */
    color: ${theme.colors.witeColor};
    position: absolute;
    left: 50%;
    transform: translateX(-50%);
    bottom: 0;
    text-transform: uppercase;
    padding: 8px 12px;
    border-radius: 4px;
    font-weight: 700;
    font-size: 16px;
    line-height: 150%;
    white-space: nowrap;
    &:before {
      content: "";
      display: block;
      position: absolute;
      top: 10px;
      bottom: 10px;
      left: -10px;
      right: -10px;
      background: black;
      z-index: -1;
    }
    &:after {
      content: "";
      display: block;
      position: absolute;
      top: 4px;
      bottom: 4px;
      left: -6px;
      right: -6px;
      background: black;
      z-index: -1;
    }
  }

  ${({ isOpen }) =>
    isOpen
      ? css`
          ${ScCard} {
            pointer-events: initial;
          }
        `
      : css`
          &:hover {
            ${ScBall} {
              border-radius: 50%;
              transform: translateY(-5%);
              box-shadow: inset -10px 10px 0 10px #ccc,
                0px 0px 64px ${hexToRGB("#FFD12E", 0.6)};
            }
          }
        `}
`;

export const ScCard = styled.div<{ isAnimate?: boolean }>`
  width: 350px;
  height: auto;
  position: fixed;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  aspect-ratio: 9/12;
  border-radius: 8px;
  box-sizing: border-box;
  padding: 46px 16px 0;
  z-index: 10;
  opacity: 0;
  pointer-events: none;

  background: url(${cardBg}) no-repeat;
  background-size: 100% 100%;
  animation: ${show} 0.5s 0.1s forwards;

  .name {
    background: transparent;
    bottom: auto;
    padding: 0;
    font-size: 12px;
    top: 20px;
  }
  .headline {
    font-size: 12px;
    font-weight: 700;
    padding: 0 8px;
    margin: 0 0 8px 0;
  }

  ul {
    padding: 0 8px;
    margin: 0 0 8px 0;
    li {
      list-style: none;
      display: flex;
      justify-content: space-between;
      margin: 0 0 6px 0;
      font-size: 12px;
    }
    .statItem {
      position: relative;
      height: 16px;
      background: ${theme.colors.secondaryColor2};
      .title {
        position: absolute;
        left: 50%;
        top: 50%;
        z-index: 3;
        transform: translateY(-50%) translateX(-50%);
      }
      .bar {
        background: #ffeb29;
        display: flex;
        position: absolute;
        left: 0;
        top: 0;
        bottom: 0;
        padding: 0 8px;
        box-sizing: border-box;
        span {
          font-weight: 700;
        }
      }
    }
  }
  & > ul {
    display: flex;
    flex-wrap: wrap;
    li {
      margin-right: 6px;
    }
  }
  .imgWrp {
    margin: 0 0 16px 0;
    img {
      width: 100%;
      height: 176px;
      object-fit: contain;
    }
  }

  ${({ isAnimate }) =>
    isAnimate &&
    css`
      transition: ease 0.3s;
      opacity: 0;
    `}
`;
