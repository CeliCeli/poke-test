import { FC } from "react";
import { ScSelect, ScSelectItem, ScOptionList, ScOption } from "./styled";
import { useState, useMemo } from "react";
import { v4 as uuid } from "uuid";

interface SelectProps {
  options: string[];
  value: string;
  onSet: (e?: any) => void;
  label?: string;
}

export const Select: FC<SelectProps> = ({
  options: list,
  value,
  onSet,
  label,
}) => {
  const id = uuid();
  const [isOpen, setIsOpen] = useState(false);

  const options = useMemo(() => {
    const arr = list;
    if (!arr) return [];
    return arr;
  }, list);

  const reasonToOpen = useMemo(() => {
    return isOpen && options.length !== 0;
  }, [isOpen, options]);

  return (
    <ScSelect isOpen={isOpen}>
      {label && <label htmlFor={id}>{label}</label>}
      <ScSelectItem>
        <input
          onClick={() => setIsOpen((s) => !s)}
          id={id}
          value={value}
          type="text"
          readOnly
        />
        {reasonToOpen && (
          <ScOptionList>
            {options.map((el, index) => (
              <ScOption
                className={(el === value && "apActive") || ""}
                id={el}
                onClick={() => {
                  setIsOpen(false);
                  onSet(el);
                }}
                key={index}
              >
                {el}
              </ScOption>
            ))}
          </ScOptionList>
        )}
      </ScSelectItem>
    </ScSelect>
  );
};
