import styled, { css, keyframes } from "styled-components/macro";
import { theme } from "theme";

export interface ScSelectProps {
  isOpen?: boolean;
}

export const ScSelect = styled.div<ScSelectProps>`
  position: relative;
  width: 100%;
  max-width: 180px;
  margin: 0 10px;
  box-shadow: 4px 0 0 0 black, -4px 0 0 0 black, 0 4px 0 0 black,
    0 -4px 0 0 black;

  label {
    font-weight: 600;
    font-size: 14px;
    line-height: 130%;
    display: block;
    margin: 0 0 12px 0;
  }
  svg {
    transition: all 0.2s;
    position: absolute;
    right: 20px;
    top: 22px;
  }

  ${({ isOpen }) =>
    isOpen &&
    css`
      svg {
        scale: -1 -1;
      }
    `}
`;

export const ScSelectItem = styled.div`
  display: flex;
  position: relative;
  background-color: ${theme.colors.secondaryColor2};
  width: 100%;

  input {
    cursor: pointer;
    height: 46px;
    width: 100%;
    background-color: transparent;
    border: none;
    outline: none;
    line-height: 130%;
    box-sizing: border-box;
    text-align: center;
    font-size: 25px;
    padding: 0;
    font-family: "VT323", monospace;
    &::selection {
      background-color: transparent;
    }
  }
`;

const show = keyframes`
    0%{
        opacity: 0;
    }
    100%{
        opacity: 1;
    }
`;

export const ScOptionList = styled.div`
  position: absolute;
  top: 64px;
  left: 0;
  right: 0;
  max-height: calc(56px * 5);
  overflow-y: scroll;
  box-shadow: 0px 20px 40px rgba(0, 0, 0, 0.1);
  opacity: 0;
  animation: ${show} 0.2s 0.1s ease forwards;
  z-index: 20;
`;

export const ScOption = styled.div`
  background-color: #fff;
  padding: 16px 20px;
  font-weight: 500;
  font-size: 16px;
  line-height: 150%;
  cursor: pointer;
  transition: 0.2s;
  &:hover {
    background-color: ${theme.colors.secondaryColor2};
  }
`;
