import styled, { keyframes } from "styled-components/macro";
import { theme } from "theme";

const shake = keyframes`
    0%{
        background-color: ${theme.colors.secondaryColor2}
    }
    50%{
        background-color: #FFE1E1;
    }
    100%{
        background-color: ${theme.colors.secondaryColor2}
    }
`;

export const ScInputGroup = styled.div`
  width: 100%;
  margin: 0 0 24px 0;
  display: flex;
  align-items: stretch;
  label {
    font-weight: 600;
    font-size: 14px;
    line-height: 130%;
    display: block;
    margin: 0 0 12px 0;
  }
  input {
    width: 100%;
    box-sizing: border-box;
    border: none;
    outline: none;
    background-color: ${theme.colors.secondaryColor2};
    height: 56px;
    padding: 0 20px;
    font-weight: 400;
    font-size: 16px;
    color: ${theme.colors.secondaryColor1};
    line-height: 130%;
    &::placeholder {
      color: ${theme.colors.secondaryColor1};
      opacity: 0.3;
    }
    &:invalid {
      animation: ${shake} 0.4s ease forwards;
    }
  }
  textarea {
    width: 100%;
    box-sizing: border-box;
    border: none;
    outline: none;
    background-color: ${theme.colors.secondaryColor2};
    padding: 16px 20px;
    font-weight: 400;
    font-size: 16px;
    color: ${theme.colors.secondaryColor1};
    line-height: 130%;
    min-height: 112px;
    max-height: 200px;
    font-family: "Inter", sans-serif;
  }
`;
