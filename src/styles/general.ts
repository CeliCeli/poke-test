import styled, { css } from "styled-components/macro";

export const ScContainer = styled.div`
  max-width: 1297px;
  padding: 0 16px;
  margin: 0 auto;
`;

interface GridProps {
  gap: number;
  grid: number;
}

export const ScGridElement = styled.div``;
export const ScGridContainer = styled.div<GridProps>`
  display: flex;
  flex-wrap: wrap;
  ${({ gap, grid }) => css`
    margin: 0 calc(-${gap}px / 2);
    ${ScGridElement} {
      flex: 0 0 calc(100% / ${grid});
      max-width: calc(100% / ${grid});
      margin-bottom: ${gap}px;
      padding: 0 calc(${gap}px / 2);
      box-sizing: border-box;
      @media (max-width: 991px) {
        flex: 0 0 calc(100% / ${grid - 1});
        max-width: calc(100% / ${grid - 1});
      }
      @media (max-width: 577px) {
        flex: 0 0 100%;
        max-width: 100%;
      }
    }
    @media (max-width: 577px){
      margin: 0;
    }
  `}
`;

export const ScSectionHeader = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  h2 {
    font-weight: 700;
    font-size: 44px;
    line-height: 130%;
    text-align: center;
    margin: 20px 0 40px;
  }
`;
