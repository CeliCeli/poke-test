import { createGlobalStyle, css } from "styled-components";
const GlobalStyle = createGlobalStyle`
   @import url('https://fonts.googleapis.com/css2?family=VT323&display=swap');
   
    body {
        margin: 0;
        padding: 0;
        font-family: 'VT323', monospace;
    } 

    img{
        max-width: 100%;
        vertical-align: bottom;
    }

    h1,h2,h3,h4,h5{
        margin: 0;
        padding: 0;
    }

    p{
        margin: 0;
    }

`;

export default GlobalStyle;
