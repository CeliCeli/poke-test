// "dd/mm/yyyy"
// "yyyy/mm/dd"
// "hh:mm"
// "dd/mm/yyyy hh:mm"

const getDate = (dt, type, divider) => {
    const currentNewDate = new Date();
  
    const devider = divider ?? "/";
  
    let d = new Date(dt ?? currentNewDate);
  
    const date = new Date(
      Date.UTC(
        d.getUTCFullYear(),
        d.getUTCMonth(),
        d.getUTCDate(),
        d.getUTCHours(),
        d.getUTCMinutes(),
        d.getUTCSeconds(),
      )
    );
  
    const addNull = (param) => {
      if (param.toString().length > 1) return param;
      else
        return `0${param}`
    }
  
    switch (type) {
      case "dd/mm/yyyy":
        return `${addNull(date.getDate())}${devider}${
          addNull(date.getMonth() + 1)
        }${devider}${date.getFullYear()}`;
      case "yyyy/mm/dd":
        return `${addNull(date.getFullYear())}${devider}${
          addNull(date.getMonth() + 1)
        }${devider}${addNull(date.getDate())}`;
      case "hh:mm":
        return `${addNull(date.getHours())}${devider}${addNull(date.getMinutes())}`;
      case "dd/mm/yyyy hh:mm":
        return `${addNull(date.getDate())}${devider}${
          addNull(date.getMonth() + 1)
        }${devider}${addNull(date.getFullYear())} ${addNull(date.getHours())}:${addNull(date.getMinutes())}`;
      default:
        return date;
    }
  };
  
  export default getDate;